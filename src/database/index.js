const sqlite3 = require('sqlite3');

const db = new sqlite3.Database("./database.sqlite3", (err) => {
  if (err) {
    console.log('Error when creating the database', err);
  } else {
    console.log('Database created!');
  }
});

const createTable = () => {
  console.log("create database table users");
  db.run("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT UNIQUE, password TEXT)");
}

createTable();

exports.db = db;
