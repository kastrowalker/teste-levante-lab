const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const database = require('./database/index');
const md5 = require('md5');

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.post('/api/login', async (req, res) => {
  const {
    email,
    password
  } = req.body;

  database.db.get("SELECT name FROM users WHERE email=$email and password=$password", {
    $email: email,
    $password: md5(password)
  }, (error, row) => {
    if (error) {
      response = {
        status: false,
        message: 'Erro ao realizar login! Tente novamente'
      }
    }

    if (row) {
      response = {
        status: true,
        message: 'Usuário logado com sucesso',
        name: row.name
      }
    } else {
      response = {
        status: false,
        message: 'Usuário ou senha incorretos! Tente novamente'
      }
    }

    return res.send(response);
  })
});

app.post('/api/register', (req, res) => {
  const {
    name,
    email,
    password
  } = req.body;

  database.db.run('INSERT INTO users(name, email, password) VALUES (?, ?, ?)', [
    name, email, md5(password)
  ], function (error) {
    if (error) {
      message = 'Erro ao criar usuário. Tente Novamente';

      if (error.errno == 19) {
        message = 'Erro ao criar usuário. O e-mail já está cadastro'
      }

      response = {
        status: false,
        message: message
      }
    } else {
      response = {
        status: true,
        message: 'Usuário criado com susesso'
      }
    }

    return res.send(response);
  });
});

app.listen(3000, () => {
  console.log('Server is up on port 3000');
});
